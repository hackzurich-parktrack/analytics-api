import analytics_api.models as AnalyticsModels


def construct_parking(model):
    if isinstance(model, AnalyticsModels.ParkingModel):
        return {"id": model.id, "name": model.name, "location": {"latitude": model.latitude, "longitude": model.longitude}}


def construct_parking(model):
    if isinstance(model, AnalyticsModels.ParkingModel):
        return {"id": model.id, "name": model.name, "location": {"latitude": model.latitude, "longitude": model.longitude}}


def select_parkings():
    parkings_query = AnalyticsModels.ParkingModel.objects.all()
    parkings = []
    for parking in parkings_query:
        parkings.append(construct_parking(parking))
    return parkings