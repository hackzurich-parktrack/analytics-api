import pandas as pd
import numpy as np
import time
from io import StringIO
import matplotlib.dates as dates

pd.options.mode.chained_assignment = None
from catboost import CatBoostRegressor, FeaturesData, Pool, CatBoostClassifier


class ComplexModel:
    schulferien = [['2019-10-07', '2019-10-18'],
                   ['2019-12-23', '2020-01-01'],
                   ['2020-02-10', '2020-02-21'],
                   ['2020-04-09', '2020-04-24'],
                   ['2020-05-21', '2020-05-22'],
                   ['2020-07-13', '2020-08-14']
                   ]

    featurevector = ['Weekday', 'Time', 'schoolHolidays', 'shoppingSunday', 'toHoliday', 'fromHoliday', 'Christmas',
                     'house_name']
    num_vector = ['Weekday', 'Time', 'schoolHolidays', 'shoppingSunday', 'toHoliday', 'fromHoliday', 'Christmas']
    cat_vector = ['house_name']

    def __init__(self, model_path):
        model = CatBoostClassifier()
        self.model = model.load_model(model_path)

        feiertage = pd.DataFrame()
        for year in range(2019, 2021):
            url = 'http://www.feiertage-schweiz.ch/kalender/{}/z%EF%BF%BDrich.html'.format(year)
            html_source = pd.read_html(url)
            html_source = html_source[2]
            html_source.columns = html_source.iloc[0]
            ind = html_source[html_source["Datum"].str.contains('Legende', na=False)].index.values[0]
            html_source = html_source.iloc[1:ind - 1]
            html_source["Datum"] = html_source["Datum"] + "." + str(year)
            html_source["Datum"] = pd.to_datetime(html_source.Datum)
            html_source = html_source.set_index("Datum")
            feiertage = feiertage.append(html_source)
        self.feiertage = feiertage

    def shoppingdaystonextfeiertag(self, df):
        diffs = []
        for feiertag in self.feiertage.index:
            diff = np.busday_count(df.date(), feiertag.date(), weekmask='Mon Tue Wed Thu Fri Sat')
            diffs.append(diff)

        try:
            return min([d for d in diffs if d >= 0])
        except:
            return 100  # in case no holiday found

    def shoppingdaysafterfeiertag(self, df):
        diffs = []
        for feiertag in self.feiertage.index:
            diff = np.busday_count(feiertag.date(), df.date(), weekmask='Mon Tue Wed Thu Fri Sat')
            diffs.append(diff)

        try:
            return min([d for d in diffs if d >= 0])
        except:
            return 100  # in case no holiday found

    def isweihnachten(self, series):
        if series.month == 12:
            return 1
        else:
            return 0

    def isoffenersonntag(self, serie):
        isoffen = False
        for offene in pd.to_datetime(['2019-12-01', '2019-12-08', '2019-12-22']):
            if serie.date() == offene.date():
                isoffen = True

        if isoffen:
            return 1
        else:
            return 0

    def read_data(self, test_data):
        test_data = StringIO(test_data)
        dataframe = pd.read_csv(test_data, names=['Date', 'house_name'], index_col='Date', parse_dates=True, sep=',')
        dataframe.sort_index(inplace=True)
        dataframe.dropna(inplace=True)
        return dataframe

    def calculate_features(self, dataframe):
        dataframe['Weekday'] = dataframe.index.dayofweek
        dataframe['Time'] = dataframe.index.hour * 60.0 + dataframe.index.minute

        sonntagsseries = pd.Series(dataframe.index, name='offeneSonntage', index=dataframe.index).apply(
            self.isoffenersonntag)
        dataframe['shoppingSunday'] = sonntagsseries

        feiertagseries = pd.Series(dataframe.index, name='Holiday', index=dataframe.index).apply(
            self.shoppingdaystonextfeiertag)
        dataframe['toHoliday'] = feiertagseries
        feiertagseries = pd.Series(dataframe.index, name='Holiday', index=dataframe.index).apply(
            self.shoppingdaysafterfeiertag)
        dataframe['fromHoliday'] = feiertagseries

        # Make scholl holidays feature
        dataframe['schoolHolidays'] = 0
        for sf in ComplexModel.schulferien:
            dataframe.loc[sf[0]:sf[1], 'schoolHolidays'] = 1

        weihnachtsseries = pd.Series(dataframe.index, name='Christmas', index=dataframe.index).apply(self.isweihnachten)
        dataframe['Christmas'] = weihnachtsseries
        return dataframe

    def predict(self, dataframe):
        data_pool = Pool(
            data=FeaturesData(
                num_feature_data=np.array(dataframe[ComplexModel.num_vector], dtype=np.float32),
                cat_feature_data=np.array(
                    dataframe[ComplexModel.cat_vector].where((pd.notnull(dataframe[ComplexModel.cat_vector])), 'None'), dtype=object),
                num_feature_names=ComplexModel.num_vector,
                cat_feature_names=ComplexModel.cat_vector
            ),
        )

        preds = self.model.predict(data_pool)
        return preds
