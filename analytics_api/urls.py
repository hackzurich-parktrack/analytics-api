from django.contrib import admin
from django.urls import path, include

from analytics_api import views

urlpatterns = [
    path('get-nearest-parking/', views.get_nearest),
    path('parkings/', views.get_parkings),
    path('parkings/<int:id>/', views.get_specific_parking),
    path('add-parkings/', views.add_parkings)
]
