from django.apps import AppConfig


class AnalyticsApiConfig(AppConfig):
    name = 'analytics_api'
