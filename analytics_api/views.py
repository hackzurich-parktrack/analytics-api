import json
from functools import cmp_to_key

import requests
from django.http import HttpResponseServerError, HttpResponse, Http404, HttpResponseBadRequest
from django.shortcuts import render

# Create your views here.
import analytics_api.models as AnalyticsModels
from django.http import JsonResponse

from datetime import datetime, timedelta

from analytics_api.ml.simple_model import SimpleModel
from analytics_api.ml.complex_model import ComplexModel
from analytics_api.utils import construct_parking, select_parkings
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

from memoize import Memoizer
from memoize import memoize, delete_memoized, delete_memoized_verhash

memoizer = Memoizer()


def get_nearest_old(request):
    if request.method == "POST":
        json_data = json.loads(request.body)
        try:
            location = json_data['location']
            latitude = float(location['latitude'])
            longitude = float(location['longitude'])
            time = json_data['time']

            model_path = "/usr/src/app/analytics_api/trained_models/simple_model"

            parkings = select_parkings()

            def distance(x):
                return ((x["location"]["latitude"] - latitude) ** 2 + (
                        x["location"]["longitude"] - longitude) ** 2) ** 0.5

            def compare(x, y):
                first = distance(x)
                second = distance(y)
                return first - second

            ordered = list(sorted(parkings, key=cmp_to_key(compare)))

            candidates = []
            for parking in ordered:
                model = SimpleModel(model_path)
                test_data = ','.join([time, parking["name"]])
                dataframe = model.calculate_features(model.read_data(test_data))
                pred = model.predict(dataframe)
                logger.warning("For %s pred is %s" % (parking, pred))
                if pred <= 90.0:
                    candidate = parking
                    candidate["distance"] = distance(candidate)
                    candidate["recommended"] = True
                    candidates.append(candidate)
                    return JsonResponse({"parkings": candidates})
                else:
                    candidate = parking
                    candidate["distance"] = distance(candidate)
                    candidate["recommended"] = False
                    candidates.append(candidate)

            return JsonResponse({"parkings": candidates})
        except KeyError:
            HttpResponseServerError("Malformed data!")
        return HttpResponse("Got json data")
    else:
        return Http404("Invalid post data")


def get_nearest(request):
    if request.method == "POST":
        json_data = json.loads(request.body)
        try:
            location = json_data['location']
            latitude = float(location['latitude'])
            longitude = float(location['longitude'])
            time = json_data['time']

            actual = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S")
            now = datetime.now() + timedelta(hours=2)  # server time, TODO: refactor to ISO format
            if now >= actual:
                res = now - actual
            else:
                res = actual - now
            if res.total_seconds() < 60 * 60:
                is_near = True
            else:
                is_near = False

            logger.warning("is_near %s" % (str(is_near),))
            model_path = "/usr/src/app/analytics_api/trained_models/complex_model"

            parkings = select_parkings()

            def distance(x, latitud, longitud):
                return ((x["location"]["latitude"] - latitud) ** 2 + (
                            x["location"]["longitude"] - longitud) ** 2) ** 0.5

            @memoize(timeout=60)
            def tomtom_distance(x, latitud, longitud):
                result = requests.get(
                    "https://api.tomtom.com/routing/1/calculateRoute/%s,%s:%s,%s/json?key=DA66As1ClS61w4UOGHq9Drqw4wYZBbOk" %
                    (latitud, longitud, x["location"]["latitude"], x["location"]["longitude"]))
                jsons = result.json()
                shortest = 10000000
                try:
                    for route in jsons["routes"]:
                        current = route["summary"]["travelTimeInSeconds"]
                        if current < shortest:
                            shortest = current
                    return shortest
                except KeyError:
                    return shortest

            def compare(x, y, distf=distance):
                first = distf(x, latitude, longitude)
                second = distf(y, latitude, longitude)
                return first - second

            def tomtom_compare(x, y):
                return compare(x, y, tomtom_distance)

            ordered = list(sorted(parkings, key=cmp_to_key(compare)))
            if (is_near):
                ordered_tomtom = ordered[:min(5, len(ordered))]
                ordered_usual = ordered[min(5, len(ordered)):]
                ordered_tomtom = list(sorted(ordered_tomtom, key=cmp_to_key(tomtom_compare)))
                ordered = ordered_tomtom
                ordered.extend(ordered_usual)

            candidates = []
            counting = 0
            for parking in ordered:
                model = ComplexModel(model_path)
                test_data = ','.join([time, parking["name"]])
                dataframe = model.calculate_features(model.read_data(test_data))
                pred = model.predict(dataframe)
                logger.warning("For %s pred is %s" % (parking, pred))
                counting += 1
                if pred == 0:
                    candidate = parking
                    candidate["distance"] = distance(candidate, latitude, longitude)
                    candidate["recommended"] = True
                    if counting <= 5 and is_near:
                        candidate["route_time"] = tomtom_distance(parking, latitude, longitude)
                    else:
                        candidate["route_time"] = "null"
                    candidates.append(candidate)
                    return JsonResponse({"parkings": candidates})
                else:
                    candidate = parking
                    candidate["distance"] = distance(candidate, latitude, longitude)
                    candidate["recommended"] = False
                    if counting < 5 and is_near:
                        candidate["route_time"] = tomtom_distance(parking, latitude, longitude)
                    else:
                        candidate["route_time"] = "null"
                    candidates.append(candidate)

            return JsonResponse({"parkings": candidates})
        except KeyError:
            HttpResponseServerError("Malformed data!")
        return HttpResponse("Got json data")
    else:
        return Http404("Invalid post data")


def get_parkings(request):
    if request.method == "GET":
        parkings = select_parkings()
        return JsonResponse({"parkings": parkings})
    else:
        return Http404("Expected GET request")


def get_specific_parking(request, id):
    if request.method == "GET":
        if isinstance(id, int):
            parking = AnalyticsModels.ParkingModel.objects.first(id=id)
            if parking is None:
                return Http404("No object found with id: %d" % id)
            return JsonResponse(construct_parking(parking))
        else:
            return HttpResponseBadRequest()
    else:
        return Http404("Expected GET request")


def add_parkings(request):
    if request.method == "POST":
        parkings_query = AnalyticsModels.ParkingModel.objects.all()
        added = 0
        for line in request.body.decode("utf-8").split('\n'):
            name, longitude, latitude = line.split(',')
            latitude = float(latitude)
            longitude = float(longitude)
            if parkings_query.filter(name=name, latitude=latitude, longitude=longitude).count() == 0:
                new_parking = AnalyticsModels.ParkingModel.objects.create(name=name, latitude=latitude,
                                                                          longitude=longitude)
                new_parking.save()
                added += 1
        return JsonResponse({"added": added})
    else:
        return Http404("Expected POST request")
