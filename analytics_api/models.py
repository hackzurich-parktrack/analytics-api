from django.db import models


class ParkingModel(models.Model):
    name = models.TextField(name="name", unique=True, blank=False, null=False, editable=True)
    longitude = models.FloatField(name="longitude", blank=False, null=False, editable=True, default=0.0)
    latitude = models.FloatField(name="latitude", blank=False, null=False, editable=True, default=0.0)
