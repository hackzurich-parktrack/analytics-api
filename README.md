# `analytics-api`

This is a Django app providing the api for parking mobile application.

## Installation

It is recommended to deploy the app using `analytics-infra`. To do this manually 
you will need to install dependencies from `requirements.txt` and setup the server by yourself.

## Usage

1. You need to add parkings you want to take into consideration. Zurich ones are stored in
    `supporting-data/zuerich-parking-info`. Just send POST request to `/api-v1.0/add-parkings/`.
    Format is:
    ```
    name,longitude,latitude
    ```
2. To get nearest parking send POST request to `/api-v1.0/get-nearest-parking/`:
    ```json
    {
        "location": {
            "latitude": 46.4211684,
            "longitude": 6.6174939
        },
        "time":  "2019-10-19T12:33:00"
    }
    ```
    We will return the data in following format:
    ```json
    {
        "parkings": [
            {
                "id": 11,
                "name": "zuerichparkhausmessezuerichag",
                "location": {
                    "latitude": 47.43774,
                    "longitude": 8.349602
                },
                "distance": 2.008386488704843,
                "recommended": true,
                "route_time": 9847
            }
        ]
    }
    ```
   We make predictions in the following way:
   1. At first, we consider if we make prediction near now or not: if distance in time between provided time and now is less than 
    an hour we will also use tomtom's data
    2. Then we sort all available parkings by exact distance.
    3. Then if we chose to use tomtom at stage 1 we sort first 5 parkings by tomtom's travel time. (5 was
    chosen not to abuse tomtom's api). 
    4. Then we iterate through these parkings in order we set up at 1-3 stages applying our model to them. If model says, that
    a parking will probably have free slots we will recommend it and return immediately. Otherwise we don't recommend it but include
    it as a possible alternative and continue iterating.
    5. Finally we return possible recommendations in json format. 
3. To get all available parkings send GET request to `/api-v1.0/parkings/`:
    ```json
    {
        "parkings": [
            {
                "id": 1,
                "name": "zuerichparkhausfeldegg",
                "location": {
                    "latitude": 47.3603828457421,
                    "longitude": 8.55352671595692
                }
            },
            ...
        ]
    }
    ```